const express = require('express');
const { Post } = require('../models/post');
const router = express.Router();

router.get(`/`, async (req, res) =>{
    const postList = await Post.find();

    if(!postList) {
        res.status(500).json({success: false})
    } 
    res.status(200).send(postList);
})

router.get('/:id', async(req,res)=>{
    const post = await Post.findById(req.params.id);

    if(!post) {
        res.status(500).json({message: 'The post with the given ID was not found.'})
    } 
    res.status(200).send(post);
})



router.post('/', async (req,res)=>{
    let post = new Post({
        post_type: req.body.post_type,
        aspect_ratio: req.body.aspect_ratio,
        tagged_people: req.body.tagged_people,
        vibe_tags: req.body.vibe_tags,
        post_location: req.body.post_location,
        post_description: req.body.post_description,
        post_url: req.body.post_url,
        post_poster_image: req.body.post_poster_image,
        is_favourite: req.body.is_favourite,
        date_created: req.body.date_created
    })
    post = await post.save();

    if(!post)
    return res.status(400).send('the post cannot be created!')

    res.send(post);
})


router.put('/:id',async (req, res)=> {
    const post = await Post.findByIdAndUpdate(
        req.params.id,
        {
            post_type: req.body.post_type,
            tagged_people: req.body.tagged_people,
            vibe_tags: req.body.vibe_tags,
            post_location: req.body.post_location,
            post_description: req.body.post_description,
            post_url: req.body.post_url,
            date_created: req.body.date_created
        },
        { new: true}
    )

    if(!post)
    return res.status(400).send('the post cannot be created!')

    res.send(post);
})

router.delete('/:id', (req, res)=>{
    Post.findByIdAndRemove(req.params.id).then(post =>{
        if(post) {
            return res.status(200).json({success: true, message: 'the post is deleted!'})
        } else {
            return res.status(404).json({success: false , message: "post not found!"})
        }
    }).catch(err=>{
       return res.status(500).json({success: false, error: err}) 
    })
})

module.exports =router;