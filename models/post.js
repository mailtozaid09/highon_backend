const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
    post_type: {
        type: String,
    },
    aspect_ratio: {
        type: String,
    },
    tagged_people: {
        type: Array,
    },
    vibe_tags: {
        type: Array,
    },
    post_location: {
        type: String,
    },
    post_description: {
        type: String,
    },
    post_url: {
        type: String,
    },
    post_poster_image: {
        type: String,
    },
    is_favourite: {
        type: Boolean,
        default: false,
    },
    date_created: {
        type: Date,
        default: Date.now,
    },
})

postSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

postSchema.set('toJSON', {
    virtuals: true,
});

exports.Post = mongoose.model('Post', postSchema);
